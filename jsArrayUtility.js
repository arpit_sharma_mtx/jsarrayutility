const items = [
    { name: 'Bike',     price: 100  },
    { name: 'TV',       price: 200  },
    { name: 'Album',    price: 10   },
    { name: 'Book',     price: 5    },
    { name: 'Phone',    price: 500  },
    { name: 'Computer', price: 1000 },
    { name: 'Keyboard', price: 25   },
]

// Reduce Method:- 

const total = items.reduce((currentTotal, item) => {
    return item.price + currentTotal
}, 0)

console.log(total) 
//result = 1840, sum of all the items price

// Filter Method:- 

const filteredItems = items.filter((item) => {
    return item.price <= 100
    // return array of those element which statisfy the fiter condition
})

console.log(filteredItems)

/*  
result: (4) [{…}, {…}, {…}, {…}]
        0: {name: "Bike", price: 100}
        1: {name: "Album", price: 10}
        2: {name: "Book", price: 5}
        3: {name: "Keyboard", price: 25}
        length: 4
        __proto__: Array(0) 
*/


// 3). Map Method;- 

const itemNames = items.map(item => {
    return item.name
    // returns an array of all item.name 
})

console.log(itemNames)

/* 
result: (7) ["Bike", "TV", "Album", "Book", "Phone", "Computer", "Keyboard"]
        0: "Bike"
        1: "TV"
        2: "Album"
        3: "Book"
        4: "Phone"
        5: "Computer"
        6: "Keyboard"
        length: 7
        __proto__: Array(0)
*/

// 4). Find Method:- 

const foundItem = items.find(item => {
    return item.name == 'Book'
    //return the matched element
})

console.log(foundItem)

/* 
result: {name: "Book", price: 5}
        name: "Book"
        price: 5
        __proto__: Object
*/

// 5). ForEach Method

items.forEach(item => {
    // like a for loop iterate over whole array
    console.log(item.name)
})

/* 
result: Bike
        TV
        Album
        Book
        Phone
        Computer
        Keyboard
*/

// 6). Some Method
// Note: None of the method modify the original array
const hasInexpensiveItems = items.some(item => {
    return item.price <= 100
    // returns boolean if any element from array matches the condition
})

console.log(hasInexpensiveItems);

// result: true

// 7). Every Method

const areAllInexpensiveItems = items.every(item => {
    return item.price <= 100
    // returns boolean if all element from array matches the condition
})

console.log(areAllInexpensiveItems);

// result: false


// 8). Includes Method

const numberList  = [1, 2, 3, 4, 5]
    
const includesTwo = numberList.includes(2) // returns boolean if array contain the element

console.log(includesTwo);

// result: true


